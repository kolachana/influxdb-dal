package io.macaw.persistence.influxdb.dal;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.influxdb.InfluxDB.LogLevel;
import org.influxdb.dto.Query;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.macaw.persistence.influxdb.dal.config.DatabaseConfig;
import io.macaw.persistence.influxdb.dal.config.RetentionPolicy;
import io.macaw.persistence.influxdb.dal.dao.InfluxdbConnectionPool;
import io.macaw.persistence.influxdb.dal.dao.InfluxdbDAO;
import io.macaw.persistence.influxdb.dal.dto.CPU;

public class DALTest {

    @BeforeClass
    public void setup() {
        Properties configProps = new Properties();
        configProps.setProperty("macaw.events.influxdb.host", "localhost");
        configProps.setProperty("macaw.events.influxdb.port", "8086");
        configProps.setProperty("macaw.events.influxdb.user", "root");
        configProps.setProperty("macaw.events.influxdb.password", "root");
        configProps.setProperty("macaw.events.influxdb.log.level", LogLevel.FULL.name());
        configProps.setProperty("macaw.events.influxdb.database", "cfxtsd");
        InfluxdbConnectionPool.getInstance().initialize(configProps);
        DatabaseConfig.getInstance().initialize(configProps);
    }

    @Test(enabled=false)
    public void testSave() {
        InfluxdbDAO dao = new InfluxdbDAO(CPU.class);

        dao.delete(new Query("delete from cpu", DatabaseConfig.getInstance().getDatabase()));
        
        List<CPU> cpus = new ArrayList<>();
        CPU cpu = new CPU();
        
        cpu.setHostname("host-1");
        cpu.setRegion("us-north");
        cpu.setUptimeSecs(900l);;
        cpu.setIdle(90d);
        cpu.setHappydevop(true);
        cpu.setTime(Instant.now());
        
        cpus.add(cpu);
        
        cpu = new CPU();
        
        cpu.setHostname("host-2");
        cpu.setRegion("us-north");
        cpu.setUptimeSecs(800l);;
        cpu.setIdle(80d);
        cpu.setHappydevop(true);
        cpu.setTime(Instant.now());
        
        cpus.add(cpu);
        
        dao.save(cpus, RetentionPolicy.oneday);
        
        String qstr = String.format("select count(idle) from %s.%s.cpu", DatabaseConfig.getInstance().getDatabase(), RetentionPolicy.oneday.name());
        
        Query query = new Query(qstr, DatabaseConfig.getInstance().getDatabase());
        
        long count = dao.getCount(query);
        
        Assert.assertEquals(count, 2);
    }
    
}
