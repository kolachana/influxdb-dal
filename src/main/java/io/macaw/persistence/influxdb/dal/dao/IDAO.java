package io.macaw.persistence.influxdb.dal.dao;

import java.util.LinkedHashMap;
import java.util.List;

import io.macaw.persistence.influxdb.dal.config.RetentionPolicy;
import io.macaw.persistence.influxdb.dal.dto.IDTO;

public interface IDAO {
    public List<? extends IDTO> findBy(LinkedHashMap<String, Object> fields);

    public void save(List<? extends IDTO> entities, RetentionPolicy policy);

    public void save(IDTO entity, RetentionPolicy policy);
}
