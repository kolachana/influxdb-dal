package io.macaw.persistence.influxdb.dal.config;

public enum RetentionPolicy {
    onehour("1h"), oneday("1d"), twodays("2d"), fivedays("5d"), week("7d"), month("30d"), quarter("90d"), year("365d");
    String duration;
    RetentionPolicy(String duration) {this.duration = duration;}
    public String duration() { return this.duration; }
};
