package io.macaw.persistence.influxdb.dal.config;

import java.util.Properties;

import org.influxdb.InfluxDB;
import org.influxdb.dto.Query;

import io.macaw.persistence.influxdb.dal.dao.InfluxdbConnectionPool;

public class DatabaseConfig {
    private static final DatabaseConfig _instance = new DatabaseConfig();
    
    String database;;
    
    public static DatabaseConfig getInstance() { return _instance; }

    public void initialize(final Properties configProps) {
        this.database = configProps.getProperty("macaw.events.influxdb.database");        
        createDatabase();
        createRetentionPolicies();
    }

    private void createRetentionPolicies() {
        InfluxDB connection = InfluxdbConnectionPool.getInstance().borrowConnection();
        try {
            for (RetentionPolicy policy : RetentionPolicy.values()) {
                String policyQueryString = String.format("CREATE RETENTION POLICY \"%s\" ON \"%s\" DURATION %s REPLICATION 1", policy.name(), database, policy.duration());
                Query query = new Query(policyQueryString, database, true);
                connection.query(query);
            }
        }
        finally {
            if (connection != null) {
                InfluxdbConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }

    public String getDatabase() {
        return this.database;
    }
    
    private void createDatabase() {
        InfluxDB connection = InfluxdbConnectionPool.getInstance().borrowConnection();
        try {
            connection.createDatabase(this.database);
        }
        finally {
            if (connection != null) {
                InfluxdbConnectionPool.getInstance().returnConnection(connection);
            }
        }
    }
}
