package io.macaw.persistence.influxdb.dal.dao;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.beanutils.PropertyUtils;
import org.influxdb.InfluxDB;
import org.influxdb.annotation.Column;
import org.influxdb.annotation.Measurement;
import org.influxdb.dto.BatchPoints;
import org.influxdb.dto.Point;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.influxdb.dto.Point.Builder;
import org.influxdb.impl.InfluxDBResultMapper;

import io.macaw.persistence.influxdb.dal.config.DatabaseConfig;
import io.macaw.persistence.influxdb.dal.config.RetentionPolicy;
import io.macaw.persistence.influxdb.dal.dto.IDTO;


public class InfluxdbDAO implements IDAO {
    
    protected String database = null;
    protected String measurement = null;
    protected Class<? extends IDTO> entityType;
    
    public InfluxdbDAO(Class<? extends IDTO> entityType) 
    {
        this.database = DatabaseConfig.getInstance().getDatabase();
        this.measurement = getMeasurement(entityType);
        this.entityType = entityType;
    }
    
    private String getMeasurement(Class<? extends IDTO> entityType) {
        Measurement m = entityType.getAnnotation(Measurement.class);
        return m.name();
    }
    
    
    public long getCount(Query countQuery) {
        InfluxDB db = InfluxdbConnectionPool.getInstance().borrowConnection();
        
        try {
            QueryResult qresult = db.query(countQuery);

            if (qresult == null) {
                return 0;
            }
            if (qresult.hasError()) {
                throw new RuntimeException(qresult.getError());
            }
            Number val = (Number) qresult.getResults().get(0).getSeries().get(0).getValues().get(0).get(1);
            return val.longValue();
        }
        finally {
            if (db != null) {
                InfluxdbConnectionPool.getInstance().returnConnection(db);
            }
        }
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public List<? extends IDTO> findBy(LinkedHashMap fields) {
        List<IDTO> entities = new ArrayList<>();
        if (fields == null || fields.isEmpty()) {
            return entities;
        }
        
        StringBuilder sb = new StringBuilder("select * from " + this.measurement  + " where ");
        int predicates = 0;
        for (Object key : fields.keySet()) {
            Object value = fields.get(key);
            if (value == null) {
                continue;
            }
            if (predicates > 0) {
                sb.append(" and ");
            }
            predicates++;
            
            sb.append(key.toString()).append(" = '").append(value.toString()).append("'");
        }
        
        sb.append(" order by time desc ");
        
        String queryString = sb.toString();
        
        return fetchEntities(entities, queryString);
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    protected List fetchEntities(List entities, String queryString) {
        InfluxDB db = InfluxdbConnectionPool.getInstance().borrowConnection();
        try {
            Query query = new Query(queryString, this.database);
            QueryResult qresult = db.query(query);
            if (qresult.hasError()) {
                throw new RuntimeException(qresult.getError());
            }
            
            InfluxDBResultMapper resultMapper = new InfluxDBResultMapper(); // thread-safe - can be reused
            List pojos = resultMapper.toPOJO(qresult, entityType);
            
            if (pojos != null && !pojos.isEmpty()) {
                entities.addAll(pojos);
                return entities;
            }
        }
        finally {
            if (db != null) {
                InfluxdbConnectionPool.getInstance().returnConnection(db);
            }
        }
        return entities;
    }
    
    
    public void delete(Query deleteQuery) {
        InfluxDB db = InfluxdbConnectionPool.getInstance().borrowConnection();
        
        try {
            QueryResult qresult = db.query(deleteQuery);

            if (qresult == null) {
                return;
            }
            if (qresult.hasError()) {
                throw new RuntimeException(qresult.getError());
            }
        }
        finally {
            if (db != null) {
                InfluxdbConnectionPool.getInstance().returnConnection(db);
            }
        }
    }


    @SuppressWarnings("rawtypes")
    public void save(List entities, RetentionPolicy policy) {        
        InfluxDB db = InfluxdbConnectionPool.getInstance().borrowConnection();
        try {
            BatchPoints batchPoints = BatchPoints.database(this.database).retentionPolicy(policy.name())
                    .consistency(InfluxDB.ConsistencyLevel.ALL).build();
            
            for (Object entity : entities) {
                Builder builder = Point.measurement(this.measurement);
                addPoints(entity, entity.getClass(), builder);
                Point point = builder.build();
                batchPoints.point(point);
            }
            
            db.write(batchPoints);
        } catch (IllegalArgumentException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            throw new RuntimeException(e);
        } finally {
            if (db != null) {
                InfluxdbConnectionPool.getInstance().returnConnection(db);
            }
        }
    }

    public void save(IDTO entity, RetentionPolicy policy) {
        List<IDTO> entities = new ArrayList<>();
        entities.add(entity);
        save(entities, policy);
    }

    @SuppressWarnings("deprecation")
    private void addPoints(Object entity, Class<?> entityType, Builder builder) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, NoSuchMethodException 
    {
        if (entityType == null) {
            return;
        }
        addPoints(entity, entityType.getSuperclass(), builder);
        for (Field field : entityType.getDeclaredFields()) {
            Column columnAnnotation = field.getAnnotation(Column.class);
            if (columnAnnotation.tag()) {
                Object value = PropertyUtils.getProperty(entity, field.getName());
                if (value != null) {
                    builder.tag(field.getName(), value.toString());
                }
            }
            else if (columnAnnotation.name().equals("time")) {
                Instant value = (Instant) PropertyUtils.getProperty(entity, field.getName());
                if (value != null) {
                    builder.time(value.toEpochMilli(), TimeUnit.MILLISECONDS);
                }
            }
            else {
                Object value = PropertyUtils.getProperty(entity, field.getName());
                if (value != null) {
                    builder.field(field.getName(), value);
                }
            }
        }
    }
}
