package io.macaw.persistence.influxdb.dal.dao;

import java.util.Properties;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDB.LogLevel;
import org.influxdb.InfluxDBFactory;

public class InfluxdbConnectionPool 
{
    private ConcurrentLinkedQueue<InfluxDB> pool;

    private ScheduledExecutorService executorService;
    
    private static final InfluxdbConnectionPool _instance = new InfluxdbConnectionPool(2, 6, 600);
    
    String dbhost; String dbport; String user; String password;
    int minIdle; int maxIdle; long validationInterval;
    LogLevel logLevel;
    
    public static InfluxdbConnectionPool getInstance() {
        return _instance;
    }
    
    public void initialize(final Properties configProps) {
        this.dbhost = configProps.getProperty("macaw.events.influxdb.host");
        this.dbport = configProps.getProperty("macaw.events.influxdb.port");
        this.user = configProps.getProperty("macaw.events.influxdb.user");
        this.password = configProps.getProperty("macaw.events.influxdb.password");
        String logLevel = configProps.getProperty("macaw.events.influxdb.log.level");
        if (logLevel != null) {
            this.logLevel = LogLevel.valueOf(logLevel);
        }
        if (this.logLevel == null) {
            this.logLevel = LogLevel.NONE;
        }
        
        // initialize pool
        initialize(minIdle);

        // check pool conditions in a separate thread
        executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleWithFixedDelay(new Runnable()
        {
            @Override
            public void run() {
                int size = pool.size();
                if (size < minIdle) {
                    int sizeToBeAdded = minIdle - size;
                    for (int i = 0; i < sizeToBeAdded; i++) {
                        pool.add(createConnection());
                    }
                } else if (size > maxIdle) {
                    int sizeToBeRemoved = size - maxIdle;
                    for (int i = 0; i < sizeToBeRemoved; i++) {
                        pool.poll();
                    }
                }
            }
        }, validationInterval, validationInterval, TimeUnit.SECONDS);
    }
    
    /**
     * Creates the pool.
     *
     * @param minIdle            minimum number of objects residing in the pool
     * @param maxIdle            maximum number of objects residing in the pool
     * @param validationInterval time in seconds for periodical checking of minIdle / maxIdle conditions in a separate thread.
     *                           When the number of objects is less than minIdle, missing instances will be created.
     *                           When the number of objects is greater than maxIdle, too many instances will be removed.
     */
    private InfluxdbConnectionPool(final int minIdle, final int maxIdle, final long validationInterval) {
        this.maxIdle = maxIdle;
        this.minIdle = minIdle;
        this.validationInterval = validationInterval;
    }

    /**
     * Gets the next free object from the pool. If the pool doesn't contain any objects,
     * a new object will be created and given to the caller of this method back.
     *
     * @return T borrowed object
     */
    public InfluxDB borrowConnection() {
        InfluxDB object;
        if ((object = pool.poll()) == null) {
            object = createConnection();
        }

        return object;
    }

    /**
     * Returns object back to the pool.
     *
     * @param object object to be returned
     */
    public void returnConnection(InfluxDB object) {
        if (object == null) {
            return;
        }

        this.pool.offer(object);
    }

    /**
     * Shutdown this pool.
     */
    public void shutdown() {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

    /**
     * Creates a new object.
     *
     * @return T new object
     */
    protected InfluxDB createConnection() {
        InfluxDB db = InfluxDBFactory.connect("http://" + dbhost + ':' + dbport);
        db.enableBatch(2000, 100, TimeUnit.MILLISECONDS);
        db.setLogLevel(InfluxDB.LogLevel.FULL);
        return db;
    }

    private void initialize(final int minIdle) {
        pool = new ConcurrentLinkedQueue<InfluxDB>();

        for (int i = 0; i < minIdle; i++) {
            pool.add(createConnection());
        }
    }
}